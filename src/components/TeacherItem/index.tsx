import React from 'react';

import whatsappIcon from '../../assets/images/icons/whatsapp.svg';

import './style.css'

function TeacherItem() {
    return (
        <article className="teacher-item">
            <header>
                <img src="https://gitlab.com/uploads/-/system/user/avatar/4650960/avatar.png?width=400" alt="Yuri Teixeira"/>
                <div>
                    <strong>Yuri Teixeira</strong>
                    <span>Matemática</span>
                </div>
            </header>
            <p>
                Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.
            </p>
            <footer>
                <p>
                    Preço/Hora
                    <strong> R$ 80,00</strong>
                </p>
                <button type="button">
                    <img src={whatsappIcon} alt="Whatsapp"/>
                    Entrar em contato
                </button>
            </footer>
        </article>
    )
}

export default TeacherItem;